package main

import "fmt"
import "os"
import "log"
import "example/cell"
import "strconv"


func main() {
    argsWithoutProg := os.Args[1:]

    if len(argsWithoutProg) != 1 {
        log.Fatal("Board missing")
    }

    boardFile := argsWithoutProg[0]

    file, err := os.Open(boardFile)

    if err != nil {
        log.Fatal("Cannot open board file ", err)
    }

    defer file.Close()

    data := make([]byte, 89)
    count, err := file.Read(data)

    if err != nil {
        log.Fatal("Cannot read board ", err)
    }

    

    if count != 89{
        log.Fatal("Failed to read exactly 89 chars")
    }

    
    board := cell.NewBoard()

    for y := 0; y<9; y++ {
        for x := 0; x < 9; x++ {
            //fmt.Println(fmt.Sprintf("reading cell %d,%d", x, y))
            index := x + y * 10
            // 32 is space, blank in my case
            if(data[index] != 32){
                //fmt.Println(fmt.Sprintf("index %d value:%d", index, data[index] - 48))
                value := data[index] - 48

                if value < 1 || value > 9 {
                    log.Fatal(fmt.Sprintf("Bad value in cell %d,%d", x+1, y+1))
                }

                if !board.SetValue(x, y, value){
                    log.Fatal("Board is invalid")
                }
            }
            
        }
	}

    alts := cell.NewStack()

    // While board is not solved
    for (board != nil || alts.Count() > 0) && !(board != nil && board.Solved()){
        if(board != nil){
            pos, options := board.LeastOptions()
            
            // log.Fatal("pos:", pos, " options:", options)
            for _, option := range options {
                alts.Push(&cell.Node{board.ToString(pos, option)})
            }
        }
        
        board, _ = cell.FromStringBoard(alts.Pop().Value)
    }

    if board == nil || !board.Solved(){
        log.Fatal("No solution found")
    }
    

        

    res, err := os.Create("result.txt")
    if err != nil {
        log.Fatal("Cannot create result file ", err)
    }

    for y := 0; y<9; y++ {
        for x := 0; x < 9; x++ {
            if board.Value(x,y) == 0 {
                res.WriteString(" ")
            } else {
                res.WriteString(strconv.Itoa(int(board.Value(x,y))))
            }
            
        }

        res.WriteString("\n")
	}

    defer res.Close()

}
