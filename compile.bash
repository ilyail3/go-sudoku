#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export GOPATH=$DIR

if [ -f "$DIR/bin/example" ]; then
    rm "$DIR/bin/example"
fi

go install example

RETVAL=$?
if [[ $RETVAL != 0 ]]; then
    exit $RETVAL
fi

if [ ! -f "$DIR/bin/example" ]; then
    echo "Compile didn't produce a file"
    exit 1
fi
