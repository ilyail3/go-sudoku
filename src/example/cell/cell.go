package cell

import "log"
import "strings"
import "strconv"

type optionsSlice []uint8

type Cell struct {
    options optionsSlice
    value uint8
}

type Board struct {
    cells [9][9]*Cell
}

type Pos struct {
    x int
    y int
}

func (self *Pos) X() int{
    return self.x
}

func (self *Pos) Y() int{
    return self.y
}

func (slice optionsSlice) Pos(value uint8) int {
    for p, v := range slice {
        if (v == value) {
            return p
        }
    }

    return -1
}

func (slice optionsSlice) Format() string {
    options := []string {}

    for _, k := range slice {
        options = append(options, strconv.Itoa(int(k)))
    }

    return strings.Join(options, ",")
}

func (self *Board) SetValue(x int, y int, value uint8) bool {
    if x < 0 || x > 8 {
        log.Fatal("Invalid x value ", x)
    }

    if y < 0 || y > 8 {
        log.Fatal("Invalid y value ", y)
    }

    if value < 1 || value > 9 {
        log.Fatal("Invalid value ", value)
    }

    self.cells[x][y].value = value
    self.cells[x][y].options = []uint8{}

    // Remove options from same row, column
    for i := 0 ; i < 9 ; i++ {
        if y != i {
            if self.cells[x][i].value == value || !self.RemoveOption(x, i, value){
                return false
            }
        }

        if x != i {
            if self.cells[i][y].value == value || !self.RemoveOption(i, y, value){
                return false
            }
        }
    }

    blockX := x / 3
    blockY := y / 3

    for ly := blockY * 3 ; ly < (blockY + 1) * 3 ; ly ++ {
        for lx := blockX * 3 ; lx < (blockX + 1) * 3 ; lx ++ {
            if !(ly == y && lx == x) {
                

                if self.cells[lx][ly].value == value || !self.RemoveOption(lx, ly, value){
                    return false
                }
            }
        }
    }

    log.Printf("set value %d, %d to %d\n", x+1, y+1, value)
    return true
}

func (self *Board) RemoveOption(x int, y int, value uint8) bool {
    if x < 0 || x > 8 {
        log.Fatal("Invalid x value ", x)
    }

    if y < 0 || y > 8 {
        log.Fatal("Invalid y value ", y)
    }

    if value < 1 || value > 9 {
        log.Fatal("Invalid value ", value)
    }

    cell := self.cells[x][y]

    if cell.value == 0 {
        
        pos := cell.options.Pos(value)

        // log.Println(value,cell.options.Format())
        plen := len(cell.options)

        if pos != -1 {
            cell.options = 
                append(cell.options[:pos], cell.options[pos+1:]...)

            log.Printf("remove option from %d, %d to %d, left:%s\n", x+1, y+1, value,cell.options.Format())

            if len(cell.options) != plen - 1{
                log.Fatal("Bad remove")
            }
        }

        if cell.options.Pos(value) != -1 {
            log.Fatal("Doble option detected")
        }

        if len(cell.options) == 1 {
            if !self.SetValue(x, y, cell.options[0]) {
                return false
            }
        }
        
    }
    
    return true
}

func (self *Board) Value(x int, y int) uint8 {
    if x < 0 || x > 8 {
        log.Fatal("Invalid x value ", x)
    }

    if y < 0 || y > 8 {
        log.Fatal("Invalid y value ", y)
    }

    return self.cells[x][y].value
}

func (self *Board) Solved() bool {
    for y := 0; y < 9; y++ {
        for x := 0; x < 9; x++ {
            if self.cells[x][y].value == 0 {
                return false
            }
        }
    }

    return true
}

func (self *Board) LeastOptions() (*Pos, []uint8){
    leastOptions := 9
    loc := new(Pos)
    
    for y := 0; y < 9; y++ {
        for x := 0; x < 9; x++ {
            if self.cells[x][y].value == 0 && len(self.cells[x][y].options) < leastOptions {
                leastOptions = len(self.cells[x][y].options)

                loc.x = x
                loc.y = y
            }
        }
    }

    return loc, self.cells[loc.x][loc.y].options
}

func (self *Board) ToString(pos *Pos, value uint8) string{
    result := ""

    for y := 0; y < 9; y++ {
        for x := 0; x < 9; x++ {
            if x == pos.X() && y == pos.Y() {
                result += strconv.Itoa(int(value))
            } else if self.cells[x][y].value != 0 {
                result += strconv.Itoa(int(self.cells[x][y].value))
            } else {
                result += " "
            }
        }
    }

    return result
}

func NewBoard() *Board{
    
    board := new(Board)   

    for y := 0; y < 9; y++ {
        for x := 0; x < 9; x++ {
            board.cells[x][y] = new(Cell)
            
            // Init with all options enabled
            board.cells[x][y].options = []uint8{1,2,3,4,5,6,7,8,9};
            board.cells[x][y].value = 0;
        }
    }

    return board
}

func FromStringBoard(boardString string) (*Board, bool){
    board := NewBoard()

    for y := 0; y < 9; y++ {
        for x := 0; x < 9; x++ {
            ind := y * 9 + x

            if boardString[ind] != 32 {
                if !board.SetValue(x, y, boardString[ind] - 48){
                    return nil, false
                }
            }
        }
    }

    return board, true
}


